/* Include core modules */
//#include "stm32f4xx.h"
/* Include my libraries here */
#include "defines.h"
#include "delay.h"
#include "tm_stm32f4_mpu6050.h"
//#define ARM_MATH_CM4
//#include "arm_math.h"


#include <stdio.h>
#include "misc.h"
#include "math.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"
#include "stm32f4xx_usart.h"
#include "stm32f4xx_exti.h"
#include "stm32f4xx_syscfg.h"
#include "acc.h"

#define PID_PARAM_KP        2            /* Proporcional */
#define PID_PARAM_KI        0.025        /* Integral */
#define PID_PARAM_KD        1            /* Derivative */


/* Private variables ---------------------------------------------------------*/
TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
TIM_OCInitTypeDef  TIM_OCInitStructure;

uint16_t PrescalerValue = 0;
uint16_t count=999;
uint32_t baudrate=9600;
volatile char received_str[11];
volatile char choice = 'q';
uint16_t speed=999;
TM_MPU6050_t MPU6050_Data;
const float alpha = 0.5;		//Smoothing factor

double fX = 0;
double fY = 0;
double fZ = 0;
AccelerometerDataStruct dat;
float pitch,roll;
float pitch_error,roll_error,desired_pitch,desired_roll;
float dutyr=0;
float dutyp=0;


/* Private function prototypes -----------------------------------------------*/
void TIM_Config(void);
void InitializeESC(void);
void Init_USART(uint32_t);
void PWM_config(void);
void USART1_IRQHandler(void);
void setcommand(char);
void CENTER(void);
void UP(void);
void DOWN(void);
void FORWARD(void);
void BACKWARD(void);
void LEFT(void);
void RIGHT(void);
void CLOCKWISE(void);
void ANTICLOCK(void);
void InitMPU6050(void);
void initBTState();
int btConnected();
void safemode();
void getPR();

/* Set PID parameters */
    /* Set this for your needs */
/*  PIDr->Kp = PID_PARAM_KP;        // Proporcional
    PIDr->Ki = PID_PARAM_KI;        // Integral 
    PIDr->Kd = PID_PARAM_KD;        // Derivative 

    PIDp->Kp = PID_PARAM_KP;        //Proporcional 
    PIDp->Ki = PID_PARAM_KI;        // Integral 
    PIDp->Kd = PID_PARAM_KD;        // Derivative 
*/


int main(void) {

float speedo;

SysTick_Init();

initAccelerometer(); // Initialize Acc

initBtState();
    
/* TIM Configuration */
  TIM_Config();

  // PWM configuration
  PWM_config();

  // Initialize USART1
   Init_USART(baudrate);

	
			
while(1){
switch(choice){

		case 'u':
			
			if(speed<1999)
		        speed+=50;
			desired_pitch=0;
			desired_roll=0;
			choice='q';
			break;

		case 'd':
			
			if(speed>999)
		        speed-=50;
			desired_pitch=0;
			desired_roll=0;
			choice='q';
			break;

		case 'f':
			desired_pitch=0;
			desired_roll=-5;
			choice='q';
			break;
		
		case 'b':
			desired_pitch=0;
			desired_roll=5;
			choice='q';
			break;	

		case 'l':
			desired_pitch=-5;
			desired_roll=0;
			choice='q';

			break;

		case 'r':
			desired_pitch=5;
			desired_roll=0;
			choice='q';

			break;
		//case 'c':
			//CLOCKWISE();
			//break;
		//case 'a':
			//ANTICLOCK();
			//break;
		case 'q':
			desired_pitch=0;
			desired_roll=0;

			break;
		case 'i':
			InitializeESC();
			break;
		case 's':
			TIM3->CCR1 = 999;
			TIM3->CCR2 = 999;
			TIM3->CCR3 = 999;
			TIM3->CCR4 = 999;
			speed = 999;
			break;

		}
		
		getPR();
		pitch_error = desired_pitch - pitch;
		dutyp = 1.5 * pitch_error;
		roll_error = desired_roll - roll;
		dutyr = 1.5 * roll_error;
		//printf("Actual Pitch: %f\tDesired Pitch: %f\tDuty Pitch: %f\n\n", pitch,desired_pitch,dutyp);
		//printf("Actual Roll: %f\tDesired Roll: %f\tDuty Roll: %f\n\n", roll,desired_roll,dutyr);

		if(speed>999){
		if(roll<0){
				speedo = speed + dutyr;
				if(speedo > speed + 25)
					speedo=speed+25;
					TIM3->CCR1 = speedo;
					TIM3->CCR4 = speedo;
		
			//printf("current speed case 1 = %f\n",speedo);
			}
		if(roll>0){
			speedo = speed - dutyr;
				if(speedo < speed - 25)
					speedo=speed-25;
					TIM3->CCR2 = speedo;
					TIM3->CCR3 = speedo;
			//printf("current speed case 2 = %f\n",speedo);		
		}
		if(pitch<0){
			speedo = speed + dutyp;
			if(speedo > speed + 25)
				speedo=speed+25;
			TIM3->CCR1 = speedo;
			TIM3->CCR2 = speedo;
		}
		if(pitch>0){
			speedo = speed - dutyp;
			if(speedo < speed - 25)
				speedo=speed-25;		
			TIM3->CCR3 = speedo;
			TIM3->CCR4 = speedo;
		}
		}
		//Little delay 
		//delay_nms(10);

			
	}

}

void getPR(){
			readAxes(&dat);	
			//setbuf(stdout, NULL);
			//printf("X: %d Y: %d Z: %d\n", dat.X, dat.Y, dat.Z);
			//lastTime = millisecondCounter;

			//Low Pass Filter (For smoothing the angles, alpha is smoothing factor)
    			fX = dat.X * alpha + (fX * (1.0 - alpha));
    			fY = dat.Y * alpha + (fY * (1.0 - alpha));
    			fZ = dat.Z * alpha + (fZ * (1.0 - alpha));
			
			//printf("fX: %f fY: %f fZ: %f\n", fX, fY, fZ);

			//Roll & Pitch Equations ( easier when converted to degrees )
    			roll  = (atan2(fY, fZ)*180.0)/M_PI; // varies from -180 to 180 degrees
    			pitch = (atan2(-fX, sqrt(fY*fY + fZ*fZ))*180.0)/M_PI; // varies from -90 to 90 degrees
			
			/* OLD Pitch and roll calculations (Tried these first but values are spiky! Need smoothing)
			//pitch = (float) atan(dat.X/sqrt((dat.Y*dat.Y) + (dat.Z*dat.Z)));  
			//roll  = (float) atan(dat.Y/sqrt((dat.X*dat.X) + (dat.Z*dat.Z)));
			*/

			//printf("Pitch: %f Roll: %f\n", pitch, roll);
}

/*void UP(void)
{

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed<1999)
		speed+=50;
		TIM3->CR1 |= (1<<1);
		
                TIM3->CCR1 = speed;
		TIM3->CCR2 = speed;
		TIM3->CCR3 = speed;
		TIM3->CCR4 = speed;
		TIM3->EGR |= (0<<1);
		TIM3->CR1 &= ~(1<<1); 

	desired_pitch=0;
	desired_roll=0;

		


		
	//}
//	printf("        speed = %d \n", speed );
	choice = 'q';
}
*/


/*oid DOWN(void)
{
int i;
//	setbuf(stdout, NULL);
//	printf("choice = %c",choice);
	if(speed>999){
		speed-=50;
		TIM3->CR1 |= (1<<1);
		
                TIM3->CCR1 = speed;
		TIM3->CCR2 = speed;
		TIM3->CCR3 = speed;
		TIM3->CCR4 = speed;
		
		TIM3->EGR |= (0<<1);
		TIM3->CR1 &= ~(1<<1);
		
	
	desired_pitch=0;
	desired_roll=0;

	}
//	printf("	speed = %d\n",speed);
	choice = 'q';
}


void CENTER(){

		desired_pitch=0;
		desired_roll=0;
//if(speed>999){
		
	        TIM3->CR1 |= (1<<1);
		TIM3->CCR1 = speed;
		TIM3->CCR2 = speed;
		TIM3->CCR3 = speed;
		TIM3->CCR4 = speed;
		TIM3->EGR |= (0<<1);
		TIM3->CR1 &= ~(1<<1);

	   while(roll>1 ){
		TIM3->CCR2 -= 6; 
		TIM3->CCR3 -= 6;
			    }

		while(roll<-1){
		TIM3->CCR2 += 10; 
		TIM3->CCR3 += 10;    
			    }

		while(pitch>2){
		TIM3->CCR1 -= 10; 
		TIM3->CCR2 -= 6;
			    }

		while(pitch<-2){
		TIM3->CCR3 -= 6; 
		TIM3->CCR4 -= 10;
		
			    }
	    

	        printf("M1=%d   M2=%d   M3=%d   M4=%d \n", TIM3->CCR1, TIM3->CCR2, TIM3->CCR3, TIM3->CCR4 );
	
	//}
}


void FORWARD(){

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed>999){
		
		TIM3->CCR1 = speed - 10;
		TIM3->CCR2 = speed + 10;
		TIM3->CCR3 = speed + 10;
		TIM3->CCR4 = speed - 10;
	}
		
	desired_pitch=0;
	desired_roll=-6;
	
	//printf("Forward\n");
	choice = 'q';

}

void BACKWARD(){

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed>999){
		
		TIM3->CCR1 = speed + 10;
		TIM3->CCR2 = speed - 10;
		TIM3->CCR3 = speed - 10;
		TIM3->CCR4 = speed + 10;
	}
	//printf("Backward\n");
	choice = 'q';

}

void LEFT(){

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed>999){
		
		TIM3->CCR1 = speed - 10;
		TIM3->CCR2 = speed - 10;
		TIM3->CCR3 = speed + 10;
		TIM3->CCR4 = speed + 10;
	}
	//printf("Left\n");
	choice = 'q';

}

void RIGHT(){

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed>999){
		
		TIM3->CCR1 = speed + 10;
		TIM3->CCR2 = speed + 10;
		TIM3->CCR3 = speed - 10;
		TIM3->CCR4 = speed - 10;
	}
	//printf("Right\n");
	choice = 'q';

}

*/
/*
void CLOCKWISE(){

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed>999){
		
		TIM3->CCR1 = speed + 50;
		TIM3->CCR2 = speed - 50;
		TIM3->CCR3 = speed + 50;
		TIM3->CCR4 = speed - 50;
	}
	//printf("Clockwise\n");
	choice = 'q';

}

void ANTICLOCK(){

	//setbuf(stdout, NULL);
	//printf("choice = %c",choice);
	if(speed>999){
		
		TIM3->CCR1 = speed - 50;
		TIM3->CCR2 = speed + 50;
		TIM3->CCR3 = speed - 50;
		TIM3->CCR4 = speed + 50;
	}
	//printf("Anti Clockwise\n");
	choice = 'q';

}
*/

void InitMPU6050(){

	
	
	
	/* Initialize system */
	SystemInit();
	
	SysTick_Init();
	
	/* Initialize MPU6050 sensor */
	if (TM_MPU6050_Init(&MPU6050_Data, TM_MPU6050_Device_0, TM_MPU6050_Accelerometer_8G, TM_MPU6050_Gyroscope_2000s) != TM_MPU6050_Result_Ok) {
		/* Display error to user */
		//setbuf(stdout, NULL);
		//printf("MPU6050 Error\n");
		
		/* Infinite loop */
		//while (1);
	}

}

void PWM_config(void)
{

  /* Compute the prescaler value */
  PrescalerValue = (uint16_t) ((SystemCoreClock /2) / 1000000) - 1;  //generate 1Mz frequency

  /* Time base configuration */
  TIM_TimeBaseStructure.TIM_Period = 20000-1;//down to 50hz
  TIM_TimeBaseStructure.TIM_Prescaler = 42-1;//42-1
  TIM_TimeBaseStructure.TIM_ClockDivision = 0;
  TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

  TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

  /* PWM1 Mode configuration: Channel1 */
  TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 1000-1;
  TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;

  TIM_OC1Init(TIM3, &TIM_OCInitStructure);

  TIM_OC1PreloadConfig(TIM3, TIM_OCPreload_Enable);

 // PWM1 Mode configuration: Channel2 
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 1000-1;

  TIM_OC2Init(TIM3, &TIM_OCInitStructure);

  TIM_OC2PreloadConfig(TIM3, TIM_OCPreload_Enable);

  //PWM1 Mode configuration: Channel3 
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 1000-1;

  TIM_OC3Init(TIM3, &TIM_OCInitStructure);

  TIM_OC3PreloadConfig(TIM3, TIM_OCPreload_Enable); 

  // PWM1 Mode configuration: Channel4 
  TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Enable;
  TIM_OCInitStructure.TIM_Pulse = 1000-1;

  TIM_OC4Init(TIM3, &TIM_OCInitStructure);

  TIM_OC4PreloadConfig(TIM3, TIM_OCPreload_Enable); 

  TIM_ARRPreloadConfig(TIM3, ENABLE);
 
}


void InitializeESC(void)
{
//setbuf(stdout, NULL);
//printf("choice = %c",choice);
	TIM3->CCR1 = 2000-1;
	TIM3->CCR2 = 2000-1;
	TIM3->CCR3 = 2000-1;
	TIM3->CCR4 = 2000-1;
	TIM_Cmd(TIM3, ENABLE);	// TIM3 enable counter 
	delay_nms(2000);	// Wait 2 secs for calibration
	//TIM3->CR1 |= (1<<1);
	TIM3->CCR1 = 1000-1;  	// Give low signal.
	TIM3->CCR2 = 1000-1;
	TIM3->CCR3 = 1000-1;
	TIM3->CCR4 = 1000-1;
	//TIM3->CR1 &= ~(1<<1);
        choice = 'q';
}




/*
    Configure the TIM3 Ouput Channels.
*/
void TIM_Config(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;

  /* TIM3 clock enable */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

  /* GPIOC and GPIOB and GPIOA clock enable */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOB, ENABLE);
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  
  /* GPIOC Configuration: TIM3 CH1 (PC6) and TIM3 CH2 (PC7) */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 ;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOC, &GPIO_InitStructure); 
  
  /* GPIOB Configuration:  TIM3 CH3 (PB0) and TIM3 CH4 (PB1) */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP ;
  GPIO_Init(GPIOB, &GPIO_InitStructure); 

// setting the USER BUTTON pin
    GPIO_StructInit(&GPIO_InitStructure);
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* Connect TIM3 pins to AF2 */  
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM3);
  GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM3); 
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource0, GPIO_AF_TIM3);
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource1, GPIO_AF_TIM3); 
}

void SysTick_Handler(void) {
	TimeTick_Decrement();
}

void Init_USART(uint32_t baudrate)
{
	
  GPIO_InitTypeDef GPIO_InitStruct; // this is for the GPIO pins used as TX and RX
  USART_InitTypeDef USART_InitStruct; // this is for the USART1 initilization
  NVIC_InitTypeDef NVIC_InitStructure; // this is used to configure the NVIC (nested vector interrupt controller)
  
  /* enable APB2 peripheral clock for USART1
  * note that only USART1 and USART6 are connected to APB2
  * the other USARTs are connected to APB1
  */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);
  
  /* enable the peripheral clock for the pins used by
  * USART1, PA9 for TX and PA10 for RX
  */
  RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
  
  /* This sequence sets up the TX and RX pins
  * so they work correctly with the USART1 peripheral
  */
  GPIO_InitStruct.GPIO_Pin = GPIO_Pin_9 | GPIO_Pin_10; 		// Pins 9 (TX) and 10 (RX) are used
  GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF; 			// the pins are configured as alternate function so the USART peripheral has access to them
  GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;		// this defines the IO speed and has nothing to do with the baudrate!
  GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;			// this defines the output type as push pull mode (as opposed to open drain)
  GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;			// this activates the pullup resistors on the IO pins
  
  GPIO_Init(GPIOA, &GPIO_InitStruct);				// now all the values are passed to the GPIO_Init() function which sets the GPIO registers
  
  /* The RX and TX pins are now connected to their AF
  * so that the USART1 can take over control of the
  * pins
  */
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); 
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1);
  
  /* Now the USART_InitStruct is used to define the
  * properties of USART1
  */
  USART_InitStruct.USART_BaudRate = baudrate;				// the baudrate is set to the value we passed into this init function
  USART_InitStruct.USART_WordLength = USART_WordLength_8b;		// we want the data frame size to be 8 bits (standard)
  USART_InitStruct.USART_StopBits = USART_StopBits_1;			// we want 1 stop bit (standard)
  USART_InitStruct.USART_Parity = USART_Parity_No;			// we don't want a parity bit (standard)
  USART_InitStruct.USART_HardwareFlowControl = USART_HardwareFlowControl_None; // we don't want flow control (standard)
  USART_InitStruct.USART_Mode = USART_Mode_Tx | USART_Mode_Rx; 		// we want to enable the transmitter and the receiver
  USART_Init(USART1, &USART_InitStruct);				// again all the properties are passed to the USART_Init function which takes care of all the bit setting
  
  
  /* Here the USART1 receive interrupt is enabled
  * and the interrupt controller is configured
  * to jump to the USART1_IRQHandler() function
  * if the USART1 receive interrupt occurs
  */
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE); 		// enable the USART1 receive interrupt
  
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;		// we want to configure the USART1 interrupts
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;	// this sets the priority group of the USART1 interrupts
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;		// this sets the subpriority inside the group
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;		// the USART1 interrupts are globally enabled
  NVIC_Init(&NVIC_InitStructure);				// the properties are passed to the NVIC_Init function which takes care of the low level stuff
  
  // finally this enables the complete USART1 peripheral
  USART_Cmd(USART1, ENABLE);
}

/**
 * @brief Set up GPIO pin connected to Bluetooth status line
 *
 * This function sets up the GPIO pin (PD1) as a digital input
 * with an interrupt enabled on any logic change. This pin will 
 * be connected to the status output of the HC-05 Bluetooth module.
 * By reading this input, we can detect whether there is a 
 * Bluetooth  connection (logical 1 on this input) or not (logical 0 on 
 * this input).
 */
void initBtState() {
    NVIC_InitTypeDef NVIC_InitStructure;
    EXTI_InitTypeDef EXTI_InitStructure;
    GPIO_InitTypeDef GPIO_InitStructure;
    
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
    
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
    GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;		
    GPIO_Init(GPIOD, &GPIO_InitStructure);
    
    SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOD, EXTI_PinSource1);
    
    EXTI_InitStructure.EXTI_Line = EXTI_Line1;             
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;    
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling; 
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;            
    EXTI_Init(&EXTI_InitStructure);                         
    
    NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn;                
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;   
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;         
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;               
    NVIC_Init(&NVIC_InitStructure);                                 
}



/**
 * @brief Gives current Bluetooth connection status
 *
 * This function returns 1 if there is currently an active 
 * Bluetooth connection (as indicated by a logical 1 on the 
 * input pin connected to the HC-05 status line). Otherwise, 
 * it returns 0.
 */
int btConnected() {
    return GPIO_ReadInputDataBit(GPIOD, GPIO_Pin_1);
}


/**
 * @brief Interrupt handler for Bluetooth status changes
 * 
 * If you have previously called initBtState(), this handler will be
 * triggered when a Bluetooth connection is made or broken.
 */
void EXTI1_IRQHandler(void) {
    if(EXTI_GetITStatus(EXTI_Line1) != RESET){
      EXTI_ClearITPendingBit(EXTI_Line1);
	//setbuf(stdout,NULL);
      //printf("Bluetooth status : %d\n",btConnected()); //Print BT status when it changes
      if(!btConnected())
	safemode();
    }
} 

void safemode(){

	speed=999;
	TIM3->CCR1 = speed;
	TIM3->CCR2 = speed;
	TIM3->CCR3 = speed;
	TIM3->CCR4 = speed;
	
//	setbuf(stdout,NULL);
//	printf("\nSafe mode triggered!! Speed changed to %d\n",speed);
}
	

void USART1_IRQHandler(void){

  // Check the Interrupt status to ensure the Rx interrupt was triggered, not Tx
  if( USART_GetITStatus(USART1, USART_IT_RXNE)){
 
    // Get the byte that was transferred in choice
    choice = USART1->DR;
    /*
      setbuf(stdout, NULL);
      printf("%s",received_str); */
    
  }

}


