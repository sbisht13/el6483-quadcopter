Project Proposal for EL6483
==========================

## Team

Team members (up to four). Write name and email address of each team member:


1. Eshan Shah (eshan.shah@nyu.edu)

2. Akash Jumrani (acj311@nyu.edu)

3. Siddharth Bisht (sb4820@nyu.edu)


## Idea

We plan to design a quad copter. We will be building a quadcopter from a scratch build kit provided by electro hub. The quad will fly with the help of brushless motors and propellers, which will be programmed using stm32f4. Initially we plan to control the bot with the help of Radio controller. Further enhance its functionality by implementing Bluetooth.The increasing popularity of quad copter designs and their use for wide range of applications these days was our prime source of motivation for the project.

## Materials

The STM32F4 Discovery board should be the primary microcontroller in your project. However, you may use other devices (e.g. a secondary microcontroller or external peripherals) if you need to (based on your project requirements).

If you plan to use any extra hardware, list each device and explain what it will be used for. If you need to buy a device, include a link to the webpage for the product you intend to purchase. If you already own a device, link to the webpage from which you purchased it.

HARDWARE:

1.       STMF32F4 Discovery board. : Available Already.
2.       ELECTRO HUB QUAD COPTER KIT: http://store.flitetest.com/electrohub-quadcopter-kit/
3.       Electronics required for the quad:
	 Emax 2213/935 KV Multi Copter Motor
	 8X4.5 EMAX Quad Copter Props
	 Emax Simon Series Multi-Rotor professional 20 Amp ESC
	 male to male connectors
	 Deans Battery connector with leads
Link:   http://www.lazertoyz.com/FT-ElectroHub-Power-Pack_p_517.html

4.       Batteries: Already Available.
5.       Radio Controller for quad copter: Already available. (Turnigy 9x)
6.       Bluetooth Module.

## Milestones

Milestones mark specific points along a project timeline. Your project should have two milestones, and you should plan to demonstrate a working prototype at each:

* 16 April 2015: At this stage, you should have a preliminary working prototype of *something*. It may not have all the functionality of your final project, but you should have something operational to show at this point. Describe what you will demonstrate during this week.

Get the motors working through STM32F4Discovery. We will show the 4 motors working properly interfaced through ESC. We might show the motors responding to the User Input from the Radio Controller.

* 7 May 2015: During finals week, you will demonstrate your completed project. What do you intend to demonstrate at this time? What features will your final project have?

Working Quadcopter with bluetooth functionality. Auto Levelling optional.

## Plan of work

 * Describe (in detail) how you plan to divide the work for your project among the team members. This is a group project, and you are all responsible for ensuring the project's successful outcome; but I want to see how you plan to divide the work amongst yourselves. Who is going to be responsible for each part of the project? Make sure to divide the work in a reasonable way.
Ordering the parts - Akash/Eshan/Siddharth.

Search for existing projects on Quadcopter - Akash/Eshan/Siddharth.

Write functions to generate desired PWM signals - Eshan.

Write code to calibrate ESC (Electronic Speed Controller) - Akash.

Bot construction - Siddharth/Akash/Eshan.

Testing that the Motors work - Siddharth.

Write code to read PWM signals from Radio Receiver - Akash/Eshan.

Write code to control the motors using these signals - Siddharth/Eshan.

Interfacing the bluetooth module on Quadcopter - Akash.

Writing code for Bluetooth functionality - Siddharth/Eshan.

Modifying the existing code to read PWM signals from Bluetooth - Akash/Eshan.

Auto levelling and Report - Akash/Eshan/Siddharth.

* You will have roughly 6 weeks to work on this project. Describe what each team member needs to accomplish in each week, and what the group as a whole needs to accomplish in each week. (Plan ahead; for example, if you need to purchase extra materials, you may need to wait a couple of weeks for those to arrive.) Indicate how many hours of work is expected from each team member in each week - be realistic.

Week 1.
Ordering the parts. (2 hrs)
Search for existing projects on Quadcopter. (2 hrs)
Write functions to generate desired PWM signals.(3 hrs)
Write code to calibrate ESC (Electronic Speed Controller). (2 hrs)

Week 2.
Bot construction (6 hrs)
Testing that the Motors work. (2 hrs)

Week 3.
Write code to read PWM signals from Radio Receiver. (4 hrs)
Write code to control the motors using these signals. (4 hrs)

Milestone 1

Week 4.
Interfacing the bluetooth module on Quadcopter. (2 hrs)
Writing code for Bluetooth functionality. (3 hrs)

Week 5.
Modifying the existing code to read PWM signals from Bluetooth. (4 hrs)

Week 6.
Auto levelling.(3 hrs)
Report.(3 hrs)

Final Submission.
