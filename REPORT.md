Final Project Report: 05/15/2015
================================

Submitted by   | Quad-Copter
-------------- | ------------- 
Siddarth Bisht |  sb4820

![](http://s18.postimg.org/i83aqb0xl/20150510_211558_1.jpg)

ABSTRACT:
=========

Achieving flight has always been one of the most challenging phenomenon. Early in the history of flight, quadcopter configurations were seen as possible solutions to some of the persistent problems in vertical flight; torque-induced control issues (as well as efficiency issues originating from the�tail rotor, which generates no useful lift) can be eliminated by counter-rotation and the relatively short blades are much easier to construct. In order to drive a quadcopter through ESC�s (Electronic Speed Controller) the main controller (STM32F4 Discovery) needs to be calibrated to accept PWM signals and then provide those signals to the motors. Further the controller can also be programmed to provide stability to the copter. So in short CONFIGURE, CALIBRATE, STABILIZE and then maybe FLY.

INTRODUCTION:
=============

A quadcopter, also called a quadrotor helicopter or quadrotor, is a multirotor helicopter that is lifted and propelled by 4 rotors. Unlike most helicopters quadcopters use two sets of identical fixed pitched propellers; two clockwise (CW) and two counter-clockwise (CCW). These use variation of RPM to control lift and torque. Control of vehicle motion is achieved by altering the rotation rate of one or more rotor discs, thereby changing its torque load and thrust/lift characteristics.
In this project signals are provided to the STM32 controller through a Bluetooth module. For that first the Bluetooth module is configured to receive signals from a Bluetooth application (available from web store) and then the controller provides the calibrated signals to the motors.
The third step and the most important feature in order to achieve flight is to configure the Gyroscope (MPU6050). The gyroscope provides the readings for the pitch, roll and yaw. The STM32 controller can also be used to provide pitch and roll but it does not provide the yaw. So just using the STM32 controller and skiping the gyroscope in order to get the quad flying without caring about the yaw movement, is also possible. But for that to work the code will have to be very robust and still stability would not be chieved in the flight. The quad will take off the ground but will soon crash.

HARDWARE:
=========

The quad was built from scratch. The frame and electronic equipment for the quad-copter were ordered online, links for the same are provided at the end of this section. The Electronics include Emax 2213/935 KV Multi Copter Motors, 8X4.5 EMAX Quad Copter Props, Emax Simon Series Multi-Rotor professional 20 Amp ESC, male to male connectors, Deans Battery connector with leads 3.5mm Connectors for ESC included with motor and Turnigy 5.25V battery. The microcontroller used was STM32F4 with ARM-Cortex-M4 processor. Apart from the parts required to run the quadcopter, a bluetooth model compatible with STM32 and a Gyroscope(MPU6050) were also used. 

Central Frame:
=============
![](http://s2.postimg.org/mu42i7921/20150419_153017_1.jpg)

Motor Used:
===========
![](http://s7.postimg.org/xiozhkgkr/20150429_164807.jpg)

Propellors Used:
===============
![](http://s29.postimg.org/uiepd3vhj/20150429_162920.jpg)

Battery Used:
=============
![](http://s7.postimg.org/kex7a3sob/T1450_3_S_11_1_V.jpg)

STM32 Micro-controller:
=======================
![](http://s22.postimg.org/szk6h8t0h/stm32f4_discovery.jpg)

Bluetooth Module:
=================
![](http://s27.postimg.org/4jdn7kf9f/20150504_174033_1.jpg)

Gyroscope:
==========
![](http://s21.postimg.org/dz35vqg47/image.jpg)

The links for ordering these parts are mentioned below:
http://www.lazertoyz.com/FT-ElectroHub-Power-Pack_p_517.html
http://store.flitetest.com/electrohub-quadcopter-kit/

Table for Pin Funtionality:
===========================

PIN |     CONFIGURED AS  |       USED FOR     |          FUNCTIONALITY
----|--------------------|--------------------|---------------------------------
PC6 |        AF          | Timer 3 Channel 1  | Providing PWM signals to Motor 1
PC7 |        AF          | Timer 3 Channel 2  | Providing PWM signals to Motor 2
PB0 |        AF          | Timer 3 Channel 3  | Providing PWM signals to Motor 3
PB1 |        AF          | Timer 3 Channel 4  | Providing PWM signals to Motor 4
PA9 |        AF          | USART 1 TX         | Transmitting data to bluetooth
PA10|        AF          | USART 1 RX         | Recieving data from bluetooth
PD1 |        IN          | EXTI1_IRQ          | Determing Bluetooth Connection

INSTRUCTIONS FOR USE:
=====================
An android application ArduinoRC available from the google store (https://play.google.com/store/apps/details?id=eu.jahnestacado.arduinorc) is used for sending control signals via bluetooth.Download and open the application and then connect it to the Bluetooth. Then select the third option (Controller mode) from the menu. On the upper right hand corner there will be a menu option; select that and then click on SELECT AXES. Configure the BT app to send the required signals, here is the configuration:

BUTTON      | COMMAND | FUNCTION
------------|---------|---------
UP ARROW    |  'u'    | Move up
DOWN ARROW  |  'd'    | Move down
RIGHT ARROW |  'r'    | Move right
LEFT ARROW  |  'l'    | Move left
TRIANGLE    |  'f'    | Move forward
SQUARE      |  'b'    | Move backward
START       |  'i'    | Calibrate ESC
SELECT      |  's'    | Stop all Motors.

The next step is to calibrate the ESC. Press START on the BT app and this will calibrate the ESC. After the ESC is calibrated then press the UP ARROW and the propellers will start to rotate but the quad won�t fly just yet, you will have to press the UP ARROW 5-6 times to get the quad in the air. After that you can control it in the desired manner as mentioned from the functions above.

SOFTWARE:
=========
1. Standard Peripheral Library
2. TM_MPU6050 library for MPU6050
3. Bluetooth functions from Lab 7
4. ACCELEROMETER functions from Lab 3

DISCUSSION:
===========
Building a quadcopter from scratch is a cumbersome task. There are several parameters that need to be kept in mind and the approach needs to be regular. The most important thing to remember is that just the code is not enough to make a quad fly. The parameters such as orientation of the wooden beams, balance of the quad after mounting the ESC, battery and the controller play an equally important role. Furthermore the wired connections are the most troublesome. Finally, if you can get a quad to get off the ground then you will need to stabilise it and configure it to receive input and respond appropriately. Its almost impossible to build a quad without the GYROSCOPE. Gyro provides us with the readings of Pitch, Roll and Yaw. The above 3 parameters are pivotal. Once you have the readings then you can write the logic for maneuvering the quad.

HARDWARE PROBLEMS:
==================

1. The concept of flying quad-copter was to rotate two motors clock wise and other two counter-clock wise and place the motors rotating in same direction exactly opposite to each other on wooden beams. But the wooden beams recieved in the kit were slightly crooked which would mis-align the motors and creat a disbalance in the flight. To solve this problem new custom built wooden beams were obtained from the hardware store as per the thickness width and length of the frame.

2. Mounting all the electronics on the central fram was crucial process, as after mounting the center of gravity of the frame should stay at the center. This was a difficult task, as a lot of things needed to be mounted on that frame, and there a lot of wires involved for different connections. This problem was solved by mounting heavy components like battery, ESC, and STM32 exactly at the center of the frame.

3. The project involved lot of connecting wires from stm32 to various modules. These connections needed to be tight as, they would come of easily due to vibrations generated by the motors. A solution of soldering wires on the board was thought, which was not feasible because the pins on stm32 are very closely spaced and soldering wires on such a board means there is definate chance of wires touching undesired pins, changing the intended functionality. This problem was solved using glue gun. Glue was put on all the pins used, and female to female connectors were mounted on these glued pins for fixed connections.

4. The most important module for this project was MPU6050(Gyroscope). The MPU 6050 was soldered with a header when it arrived, this soldering was very unstable. Hence it would easily get disconnected from the board. This problem could not be solved due to time and resource limitations.

SOFTWARE PROBLEMS:
==================

In the code the quadcopter was controlled by 4 channels of the same timer on board. Whenever a new signal was recieved the values in the channels were updated to provide desired speed to motors in order to achieve required functionality. The problem here can be understood by the following lines of pseudo code:
```
if(choice = 'u') {
TIM->CCR1 = SPEED + 100;
TIM->CCR2 = SPEED + 100;
TIM->CCR3 = SPEED + 100;
TIM->CCR4 = SPEED + 100;
.
```
This code increases the speed of all 4 motors by a step of 100 when up signal is recieved. So on the board Channel 1 is written to first and than channels 2 , 3 and 4 respectively. This would add a delay in wirting to channel 4 and would tilt the quad-copter in the direction of channel 4. Once the quad is tilted it is impossible to bring it back without implementing the logic for pitch roll and yaw. One  solution to this problem was to reduce the step size from 100 to 25, but it still tilted the quadcopter and it would eventually crash on the side of motor 4. Another approach used to solve this problem was a for loop was implemented and speeds of the motors were incremented by '1' in every iteration of for loop, by doing so the difference in speed of motors would be maximum of 1 at any given time and this should not tilt for the motors. But the additonal delay of 400 Systicks was added and because of that the system would act in a wierd way when subsequent requests for up were recived. Hence one way to solve this problem was to be able to wirte to 4 channels simultaneously. But the best way to solve this problem would be implmenting a gyroscope which would constantly calculate pitch, roll and yaw and stablise the system accordingly whenever it is tilted in one direction. 

CONCLUSION:
===========

Quad-copter cannot be succesfully built throught STM32F4 alone, there is need for other microcontrollers, which would help in stablizing the flight. But the down-side of adding another microcontroller is to account for additonal space and connections on the centeral frame which is also a difficult task. When motors rotate at a very high speed during the flight, even the tiniest bug/glitch can result in dis-balance and would cause the quad-copter to crash. Hence there are only two options, either find a perfect solution to all the problems mentioned above, or implement PID control using GYROSCOPE which would take care of the slight disbalance during the flight.
